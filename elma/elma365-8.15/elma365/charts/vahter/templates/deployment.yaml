apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "vahter.name" . }}
  labels:
   {{- include "vahter.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  {{- if not .Values.global.autoscaling.enabled }}
  replicas: {{ if or (kindIs "float64" .Values.replicaCount) (kindIs "int64" .Values.replicaCount) }}{{ .Values.replicaCount }}{{ else }}{{ .Values.global.replicaCount }}{{ end }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "vahter.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      labels:
        {{- include "vahter.selectorLabels" . | nindent 8 }}
      annotations:
        {{- with .Values.global.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}        
    spec:
      imagePullSecrets:
        {{- range .Values.global.image.pullSecret }}
        - name: {{ . }}
        {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.global.image.repository }}/elma365/vahter/vahter:{{ .Values.images.vahter }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          ports:
            - name: grpc
              containerPort: {{ .Values.global.grpc_port }}
            - name: http
              containerPort: {{ .Values.global.http_port }}
        {{- if .Values.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.livenessProbe | nindent 12 }}
        {{- else if .Values.global.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.global.livenessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.readinessProbe | nindent 12 }}
        {{- else if .Values.global.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.global.readinessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.startupProbe }}
          startupProbe:
          {{- toYaml .Values.startupProbe | nindent 12 }}
        {{- else if .Values.global.startupProbe }}
          startupProbe:
          {{- toYaml .Values.global.startupProbe | nindent 12 }}
        {{- end }}
          env:
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: ELMA365_NAMESPACE
              value: {{ default .Release.Namespace .Values.namespace }} # для редиса
            - name: ELMA365_VAHTER_ALLOW_INSECURE_COOKIE
              value: {{ .Values.appconfig.allowInsecureCookie | quote }}
            - name: ELMA365_VAHTER_EASY_AUTH_TOKEN_TTL
              value: {{ .Values.appconfig.easyAuthTokenTTL }}
            - name: ELMA365_CORS_ALLOWED_HOSTS
              value: {{ join "," (pluck .Values.global.env .Values.appconfig.corsHosts | first | default .Values.appconfig.corsHosts._default ) | quote }}
            - name: ELMA365_VAHTER_PRIVATE_KEY
              valueFrom:
                secretKeyRef:
                  name: vahter-jwt
                  key: tls.key
            - name: ELMA365_VAHTER_JWT_CERTIFICATE
              valueFrom:
                secretKeyRef:
                  name: vahter-jwt
                  key: tls.crt
            - name: ELMA365_ACTIVATE_TIMEOUT
              value: {{ .Values.appconfig.activateTimeout }}
            - name: MONGO_URL
              valueFrom:
                secretKeyRef:
                  name: elma365-db-connections
                  key: VAHTER_MONGO_URL
                  optional: true
            - name: REDIS_URL
              valueFrom:
                secretKeyRef:
                  name: elma365-db-connections
                  key: REDIS_URL
                  optional: true
            - name: AMQP_URL
              valueFrom:
                secretKeyRef:
                  name: elma365-db-connections
                  key: AMQP_URL
                  optional: true
            - name: ELMA365_MIGRATION_COLLECTION_PREFIX
              value: {{ .Values.mongodb.migrationCollectionPrefix }}
            - name: ELMA365_COLLECTION_NAME_PREFIX
              value: {{ .Values.mongodb.collectionNamePrefix | quote }}
            - name: ELMA365_APPEND_SERVICE_NAME_TO_PREFIX
              value: {{ .Values.mongodb.appendServiceNameToPrefix | quote }}
            - name: ELMA365_VAHTER_SECOND_FACTOR_MAX_FAILS_COUNT
              value: {{ .Values.appconfig.secondFactor.limitAttempts | quote }}
            - name: ELMA365_VAHTER_SECOND_FACTOR_CODE_LENGTH
              value: {{ .Values.appconfig.secondFactor.codeLength | quote }}
            - name: ELMA365_VAHTER_SECOND_FACTOR_CODE_LIFETIME
              value: {{ .Values.appconfig.secondFactor.codeLifetime | quote }}
            - name: ELMA365_VAHTER_SECOND_FACTOR_EXCLUDED_IP_NETS
              value: {{ .Values.appconfig.secondFactor.excludedIPNets | quote }}
            - name: ELMA365_VAHTER_VERIFICATION_PROVIDER_TEST_CODE
              value: {{ .Values.appconfig.verificationProvider.testCode | quote }}
            - name: ELMA365_VAHTER_EXTENDED_AUTH_CODE_TTL
              value: {{ .Values.appconfig.extendedAuth.codeTTL | quote  }}
            - name: ELMA365_VAHTER_EXTENDED_AUTH_MAX_FAILS_COUNT
              value: {{ .Values.appconfig.extendedAuth.maxFailsCount | quote  }}
            #
            # Устаревшие параметры
            # Используются в onpremise. Выпилить после обновления onpremise.
            #
            - name: MONGO_DB_NAME
              value: {{ .Values.mongodb.dbName | quote }}
            - name: MONGO_USER
              valueFrom:
                secretKeyRef:
                  name: elma365-db-connections
                  key: ELMA365_MONGO_USER
                  optional: true
            - name: MONGO_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: elma365-db-connections
                  key: ELMA365_MONGO_PASSWORD
                  optional: true
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
    {{- with .Values.global.affinity }}
{{ toYaml . | indent 8 }}
    {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "vahter.name" . }}
                    - key: release
                      operator: In
                      values:
                        - "{{ .Release.Name }}"
                topologyKey: kubernetes.io/hostname
              weight: 10
    {{- end }}
